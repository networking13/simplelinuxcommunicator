// Client side C/C++ program to demonstrate Socket
// programming
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#define PORT 8080

int main(int argc, char const* argv[])
{
	int status, valread, client_fd;
	struct sockaddr_in serv_addr, my_addr;
	char buffer[1024] = { 0 };
	char* hello = "Hello from client: ";	
	unsigned char mip_str[ INET_ADDRSTRLEN ];
	char* hello_with_address = malloc(strlen(hello) + sizeof(mip_str));
	char message[1000];



	if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
		return -1;
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	// Convert IPv4 and IPv6 addresses from text to binary
	// form
	if (inet_pton(AF_INET, "192.168.1.18", &serv_addr.sin_addr)
		<= 0) {
		printf(
			"\nInvalid address/ Address not supported \n");
		return -1;
	}

	if ((status
		= connect(client_fd, (struct sockaddr*)&serv_addr,
				sizeof(serv_addr)))
		< 0) {
		printf("\nConnection Failed \n");
		return -1;
	}
	socklen_t len = sizeof(my_addr);
    getsockname(client_fd, (struct sockaddr *) &my_addr, &len);
	inet_ntop(PF_INET, &(my_addr.sin_addr), mip_str, INET_ADDRSTRLEN);
	strcpy(hello_with_address, hello); /* copy name into the new var */
	strcat(hello_with_address, mip_str);
	send(client_fd, hello_with_address, strlen(hello_with_address), 0);
	printf("Hello message sent\n");
	valread = read(client_fd, buffer, 1024);
	printf("%s\n", buffer);

	for(;;)
	{
		valread = read(client_fd, buffer, 1024);
		printf("%s\n", buffer);
		memset(buffer, '\0', sizeof(buffer));
	}

	// closing the connected socket
	close(client_fd);
	return 0;
}
